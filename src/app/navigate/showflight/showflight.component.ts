import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/customer.service';
import { Travellers } from '../travellers';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-showflight',
  templateUrl: './showflight.component.html',
  styleUrls: ['./showflight.component.css'],
})
export class ShowflightComponent implements OnInit {
  travel: Travellers[] = [];

  ngOnInit(): void {
    this.getBookings();
  }
  constructor(
    private showbooking: CustomerService,
    private router: Router,
    private toast: ToastrService
  ) {}

  private getBookings() {
    this.showbooking.getBookings().subscribe((data) => {
      this.travel = data;
    });
  }
  updateflightBooking(id: number) {
    this.router.navigate(['navigate/upflightbookings', id]);
  }
  deleteFlightBooking(id: number) {
    this.showbooking.deleteFlightById(id).subscribe((data) => {
      console.log(data);
      this.toast.error('Booking Deleted');
      this.getBookings();
    });
  }
  viewbooking(id: number) {
    this.toast.info('View Your Booking');
    this.router.navigate(['navigate/viewflbookings', id]);
  }

  totalLength: any; //Total number of items
  page: number = 1; //Current page
}
