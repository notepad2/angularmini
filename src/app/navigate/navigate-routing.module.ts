import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { BusComponent } from './bus/bus.component';
import { FlightComponent } from './flight/flight.component';
import { CarComponent } from './car/car.component';
import { ShowflightComponent } from './showflight/showflight.component';
import { ShowcarbookinComponent } from './showcarbookin/showcarbookin.component';
import { UpdatecarBookingComponent } from './updatecar-booking/updatecar-booking.component';
import { ReviewComponent } from './review/review.component';
import { UpdateFlightBookingComponent } from './update-flight-booking/update-flight-booking.component';
import { ViewflbookingComponent } from './viewflbooking/viewflbooking.component';
import { ViewcarBookingComponent } from './viewcar-booking/viewcar-booking.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      { component: BusComponent, path: 'bus' },
      { component: FlightComponent, path: 'flight' },
      { component: CarComponent, path: 'car' },
      { component: ShowflightComponent, path: 'showflight' },
      { component: ShowcarbookinComponent, path: 'showcar' },
      { component: UpdatecarBookingComponent, path: 'ucarbookings/:id' },
      { component: UpdateFlightBookingComponent, path: 'upflightbookings/:id' },
      { component: ReviewComponent, path: 'review' },
      { component: ViewflbookingComponent, path: 'viewflbookings/:id' },
      { component: ViewcarBookingComponent, path: 'viewcBooking/:id' },
      {
        path: '',
        redirectTo: 'about',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: 'home',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NavigateRoutingModule {}
