import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewcarBookingComponent } from './viewcar-booking.component';

describe('ViewcarBookingComponent', () => {
  let component: ViewcarBookingComponent;
  let fixture: ComponentFixture<ViewcarBookingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ViewcarBookingComponent]
    });
    fixture = TestBed.createComponent(ViewcarBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
