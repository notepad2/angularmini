import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/customer.service';
import { Car } from '../car';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-viewcar-booking',
  templateUrl: './viewcar-booking.component.html',
  styleUrls: ['./viewcar-booking.component.css'],
})
export class ViewcarBookingComponent implements OnInit {
  car: Car = new Car();

  constructor(
    private ViewCBooking: CustomerService,
    private route: ActivatedRoute,
    private toast: ToastrService,
    private router: Router
  ) {}

  id4: number = 0;
  ngOnInit(): void {
    this.id4 = this.route.snapshot.params['id'];
    this.ViewCBooking.getCarRentalById(this.id4).subscribe((data) => {
      this.car = data;
    });
  }
  updateflightBooking(id: number) {
    this.toast.warning('Edit Your Car Booking');
    this.router.navigate(['navigate/ucarbookings', id]);
  }
  deleteFlightBooking(id: number) {
    this.ViewCBooking.deleteCarRental(id).subscribe((data) => {
      console.log(data);
      this.toast.error('Car Rental Booking Deleted');
      this.router.navigate(['navigate/flight']);
    });
  }
  bookingPage() {
    this.router.navigate(['navigate/car']);
    this.toast.info('Booking Page');
  }
}
