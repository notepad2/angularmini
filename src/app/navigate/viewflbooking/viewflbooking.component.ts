import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from 'src/app/customer.service';
import { Travellers } from '../travellers';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-viewflbooking',
  templateUrl: './viewflbooking.component.html',
  styleUrls: ['./viewflbooking.component.css'],
})
export class ViewflbookingComponent implements OnInit {
  reviewBooking() {
    this.router.navigate(['navigate/review']);
  }
  constructor(
    private route: ActivatedRoute,
    private viewfbooking: CustomerService,
    private router: Router,
    private toast: ToastrService
  ) {}

  travel: Travellers = new Travellers();
  id5: number = 0;
  ngOnInit(): void {
    this.id5 = this.route.snapshot.params['id'];
    this.viewfbooking.getFlightDetailById(this.id5).subscribe((data) => {
      this.travel = data;
    });
  }
  deleteFlightBooking(id: number) {
    this.viewfbooking.deleteFlightById(id).subscribe((data) => {
      console.log(data);
      this.toast.error('Flight Booking Deleted');
      this.router.navigate(['navigate/flight']);
    });
  }
  updateflightBooking(id: number) {
    this.toast.warning('Edit your Booking');
    this.router.navigate(['navigate/upflightbookings', id]);
  }
  goToBookingPage() {
    this.router.navigate(['navigate/flight']);
  }
}
