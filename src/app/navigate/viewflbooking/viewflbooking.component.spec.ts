import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewflbookingComponent } from './viewflbooking.component';

describe('ViewflbookingComponent', () => {
  let component: ViewflbookingComponent;
  let fixture: ComponentFixture<ViewflbookingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ViewflbookingComponent]
    });
    fixture = TestBed.createComponent(ViewflbookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
