import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavigateRoutingModule } from './navigate-routing.module';
import { HomeComponent } from './home/home.component';
import { BusComponent } from './bus/bus.component';
import { FlightComponent } from './flight/flight.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CarComponent } from './car/car.component';
import { ShowflightComponent } from './showflight/showflight.component';
import { ShowcarbookinComponent } from './showcarbookin/showcarbookin.component';
import { UpdatecarBookingComponent } from './updatecar-booking/updatecar-booking.component';
import { ReviewComponent } from './review/review.component';
import { UpdateFlightBookingComponent } from './update-flight-booking/update-flight-booking.component';
import { ViewflbookingComponent } from './viewflbooking/viewflbooking.component';
import { ToastrModule } from 'ngx-toastr';
import { ViewcarBookingComponent } from './viewcar-booking/viewcar-booking.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    HomeComponent,
    BusComponent,
    FlightComponent,
    CarComponent,
    ShowflightComponent,
    ShowcarbookinComponent,
    UpdatecarBookingComponent,
    ReviewComponent,
    UpdateFlightBookingComponent,
    ViewflbookingComponent,
    ViewcarBookingComponent,
  ],
  imports: [
    CommonModule,
    NavigateRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    ToastrModule.forRoot({
      positionClass: 'toastr-top-right',
      preventDuplicates: true,
      timeOut: 1000,
    }),
  ],
})
export class NavigateModule {}
