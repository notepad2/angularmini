import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-bus',
  templateUrl: './bus.component.html',
  styleUrls: ['./bus.component.css'],
})
export class BusComponent implements OnInit {
  constructor(
    private passengerservice: CustomerService,
    private toast: ToastrService
  ) {}

  passengerForm = new FormGroup({
    frm: new FormControl('', [Validators.required]),
    tom: new FormControl('', Validators.required),
    nop: new FormControl('', [Validators.required, Validators.minLength(1)]),
    name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new FormControl('', [Validators.required, Validators.email]),
  });
  get name() {
    return this.passengerForm.get('name');
  }
  get fromp() {
    return this.passengerForm.get('frm');
  }
  get top() {
    return this.passengerForm.get('tom');
  }
  get nopp() {
    return this.passengerForm.get('nop');
  }
  get email() {
    return this.passengerForm.get('email');
  }
  ngOnInit(): void {}
  savepassengers() {
    this.passengerservice.passengerDetails(this.passengerForm.value).subscribe(
      (data) => {
        console.log(data);
        this.toast.success('Booking SuccessFul..');
        alert('Booking Successful');
      },
      (error) => console.error()
    );
  }
}
