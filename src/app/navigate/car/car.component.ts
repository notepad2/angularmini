import { Component, OnInit } from '@angular/core';
import { Car } from '../car';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/customer.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css'],
})
export class CarComponent implements OnInit {
  constructor(
    private cardetail: CustomerService,
    private router: Router,
    private toast: ToastrService
  ) {}
  car: Car = new Car();
  cardetails = new FormGroup({
    nameOfCS: new FormControl('', [Validators.required]),
    fromDate: new FormControl('', Validators.required),
    toDate: new FormControl('', [Validators.required]),
    nameOfCar: new FormControl('', [Validators.required]),
  });
  get nameOfCS() {
    return this.cardetails.get('nameOfCS');
  }
  get fromDate() {
    return this.cardetails.get('fromDate');
  }
  get toDate() {
    return this.cardetails.get('toDate');
  }
  get nameOfCar() {
    return this.cardetails.get('nameOfCar');
  }
  ngOnInit(): void {}

  saveCars() {
    this.cardetail.carDetails(this.cardetails.value).subscribe((data) => {
      console.log(data);
      this.router.navigate(['navigate/showcar']);
      this.toast.success('Car Booked Successfully');
    });
  }
}
