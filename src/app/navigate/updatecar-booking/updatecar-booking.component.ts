import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from 'src/app/customer.service';
import { Car } from '../car';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-updatecar-booking',
  templateUrl: './updatecar-booking.component.html',
  styleUrls: ['./updatecar-booking.component.css'],
})
export class UpdatecarBookingComponent implements OnInit {
  cardetails = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    nameOfCS: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    fromDate: new FormControl('', Validators.required),
    toDate: new FormControl('', [Validators.required]),
    nameOfCar: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
  });

  get nameOfCS() {
    return this.cardetails.get('nameOfCS');
  }
  get fromDate() {
    return this.cardetails.get('fromDate');
  }
  get toDate() {
    return this.cardetails.get('toDate');
  }
  get nameOfCar() {
    return this.cardetails.get('nameOfCar');
  }
  saveUpdatedCars() {
    this.cardetail.updateCarById(this.id1, this.cardetails.value).subscribe(
      (data) => {
        this.router.navigate(['navigate/viewcBooking', this.id1]);
        this.toast.success('Updatd Details');
      },
      (error) => console.log(error)
    );
  }
  constructor(
    private cardetail: CustomerService,
    private route: ActivatedRoute,
    private router: Router,
    private toast: ToastrService
  ) {}

  id1: number = 0;
  ngOnInit(): void {
    this.id1 = this.route.snapshot.params['id'];
    this.cardetail.getCarRentalById(this.id1).subscribe(
      (data) => {
        console.log('car name' + data.id);
        this.car = data;
      },
      (error) => console.log(error)
    );
  }
  car: Car = new Car();

  goToCarBookingList() {
    this.router.navigate(['navigate/showcar']);
  }
}
