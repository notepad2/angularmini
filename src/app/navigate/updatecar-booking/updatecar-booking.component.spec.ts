import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatecarBookingComponent } from './updatecar-booking.component';

describe('UpdatecarBookingComponent', () => {
  let component: UpdatecarBookingComponent;
  let fixture: ComponentFixture<UpdatecarBookingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UpdatecarBookingComponent]
    });
    fixture = TestBed.createComponent(UpdatecarBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
