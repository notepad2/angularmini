import { Component, OnInit } from '@angular/core';
import { Car } from '../car';
import { CustomerService } from 'src/app/customer.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-showcarbookin',
  templateUrl: './showcarbookin.component.html',
  styleUrls: ['./showcarbookin.component.css'],
})
export class ShowcarbookinComponent implements OnInit {
  car: Car[] = [];
  ngOnInit(): void {
    this.getCarRentals();
  }
  private getCarRentals() {
    this.rentalDetails.getCarRentals1().subscribe((datas) => {
      this.car = datas;
    });
  }

  constructor(
    private rentalDetails: CustomerService,
    private route: Router,
    private toast: ToastrService
  ) {}

  updateCBooking(id: number) {
    this.route.navigate(['navigate/ucarbookings', id]);
    this.toast.warning('Edit Your Booking');
  }
  deleteBooking(id: number) {
    this.rentalDetails.deleteCarRental(id).subscribe((data) => {
      console.log(data);
      this.toast.error('Booking Deleted');
      this.getCarRentals();
    });
  }
  viewCarBooking(id: number) {
    this.toast.info('Your Booking Details');
    this.route.navigate(['navigate/viewcBooking', id]);
  }
  page: number = 1;
  length: any;
}
