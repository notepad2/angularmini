import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowcarbookinComponent } from './showcarbookin.component';

describe('ShowcarbookinComponent', () => {
  let component: ShowcarbookinComponent;
  let fixture: ComponentFixture<ShowcarbookinComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ShowcarbookinComponent]
    });
    fixture = TestBed.createComponent(ShowcarbookinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
