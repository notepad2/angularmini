import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Travellers } from '../travellers';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerService } from 'src/app/customer.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-flight-booking',
  templateUrl: './update-flight-booking.component.html',
  styleUrls: ['./update-flight-booking.component.css'],
})
export class UpdateFlightBookingComponent implements OnInit {
  travel: Travellers = new Travellers();

  flightMembers = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    source: new FormControl('', [Validators.required]),
    detination: new FormControl('', Validators.required),
    numberOp: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required]),
  });
  get name() {
    return this.flightMembers.get('name');
  }
  get email() {
    return this.flightMembers.get('email');
  }
  get source() {
    return this.flightMembers.get('source');
  }
  get destination() {
    return this.flightMembers.get('detination');
  }
  get numberOp() {
    return this.flightMembers.get('numberOp');
  }
  get date() {
    return this.flightMembers.get('date');
  }

  constructor(
    private route: ActivatedRoute,
    private flightBooking: CustomerService,
    private router: Router,
    private toast: ToastrService
  ) {}

  id2: number = 0;
  ngOnInit(): void {
    this.id2 = this.route.snapshot.params['id'];
    this.flightBooking.getFlightDetailById(this.id2).subscribe(
      (data) => {
        this.travel = data;
        console.log(data.source);
      },
      (error) => console.log(error)
    );
  }
  saveUFlightBooking() {
    this.flightBooking.UpFlightBooking(this.id2, this.travel).subscribe(
      (data) => {
        this.toast.success('Details Updated');
        this.router.navigate(['navigate/viewflbookings', this.id2]);
      },
      (error) => console.log(error)
    );
  }
}
