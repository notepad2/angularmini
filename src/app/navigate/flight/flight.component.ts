import { Component, OnInit } from '@angular/core';
import { Travellers } from '../travellers';
import { CustomerService } from 'src/app/customer.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css'],
})
export class FlightComponent implements OnInit {
  constructor(
    private flightdetails: CustomerService,
    private router: Router,
    private toast: ToastrService
  ) {}
  ngOnInit(): void {}

  flightMembers = new FormGroup({
    source: new FormControl('', [Validators.required]),
    detination: new FormControl('', Validators.required),
    name: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(10),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email,
      Validators.minLength(4),
    ]),
    numberOp: new FormControl('', [
      Validators.required,
      Validators.maxLength(10),
    ]),
    date: new FormControl('', [Validators.required]),
  });
  get source() {
    return this.flightMembers.get('source');
  }
  get destination() {
    return this.flightMembers.get('detination');
  }
  get name() {
    return this.flightMembers.get('name');
  }
  get email() {
    return this.flightMembers.get('email');
  }
  get numberOp() {
    return this.flightMembers.get('numberOp');
  }
  get date() {
    return this.flightMembers.get('date');
  }

  saveTravellers() {
    this.flightdetails.travelDetails(this.flightMembers.value).subscribe(
      (data) => {
        console.log(data);
        this.router.navigate(['navigate/showflight']);
        this.toast.success(
          'Click Only on Your Details to View',
          'Booking Successful'
        );
      },
      (error) => console.error()
    );
  }
}
