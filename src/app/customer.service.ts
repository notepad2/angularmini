import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from './employee';
import { Passenger } from './navigate/passenger';
import { Car } from './navigate/car';
import { Travellers } from './navigate/travellers';

@Injectable({
  providedIn: 'root',
})
export class CustomerService {
  private baseUrl = 'http://localhost:8080/api/v1/customers';
  private baseUrl1 = 'http://localhost:8080/api/v1/login';
  private baseUrl2 = 'http://localhost:8080/api/v1/passengers';
  private baseUrl3 = 'http://localhost:8080/api/v1/car';
  private baseUrl4 = 'http://localhost:8080/api/v1/flight';

  constructor(private httpCLient: HttpClient) {}

  // Get SignUp Customers
  getCustomersList(): Observable<Employee[]> {
    return this.httpCLient.get<Employee[]>(`${this.baseUrl}`);
  }
  // Get Flight Details Full/Id/
  getBookings(): Observable<Travellers[]> {
    return this.httpCLient.get<Travellers[]>(`${this.baseUrl4}`);
  }
  getFlightDetailById(id: number): Observable<Travellers> {
    return this.httpCLient.get<Travellers>(`${this.baseUrl4}/${id}`);
  }

  UpFlightBooking(id: number, travel: Travellers): Observable<object> {
    return this.httpCLient.put(`${this.baseUrl4}/${id}`, travel);
  }
  deleteFlightById(id: number): Observable<object> {
    return this.httpCLient.delete(`${this.baseUrl4}/${id}`);
  }
  // Get ViewOf FlightBooking

  //  GET CAR RENTALS FULL/ID/ U ID / D ID
  getCarRentals1(): Observable<Car[]> {
    return this.httpCLient.get<Car[]>(`${this.baseUrl3}`);
  }
  getCarRentalById(id: number): Observable<Car> {
    return this.httpCLient.get<Car>(`${this.baseUrl3}/${id}`);
  }
  updateCarById(id: number, car: any): Observable<object> {
    return this.httpCLient.put(`${this.baseUrl3}/${id}`, car);
  }
  deleteCarRental(id: number): Observable<object> {
    return this.httpCLient.delete(`${this.baseUrl3}/${id}`);
  }

  //SignUp Customers
  createCustomer(employee: any): Observable<object> {
    return this.httpCLient.post(`${this.baseUrl}`, employee);
  }

  //Login page
  loginUser(user: any): Observable<object> {
    console.log(user);
    return this.httpCLient.post(`${this.baseUrl1}`, user);
  }
  passengerDetails(passenger1: any): Observable<object> {
    return this.httpCLient.post(`${this.baseUrl2}`, passenger1);
  }
  carDetails(car: any): Observable<object> {
    return this.httpCLient.post(`${this.baseUrl3}`, car);
  }

  // Create Flight Booked Details
  travelDetails(tavell: any): Observable<object> {
    return this.httpCLient.post(`${this.baseUrl4}`, tavell);
  }
}
