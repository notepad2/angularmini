import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { CustomerService } from '../customer.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup1',
  templateUrl: './signup1.component.html',
  styleUrls: ['./signup1.component.css'],
})
export class Signup1Component implements OnInit {
  signForm = new FormGroup({
    firstName: new FormControl('', [
      Validators.required,
      Validators.minLength(3),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email,
      Validators.minLength(8),
    ]),
  });
  get names() {
    return this.signForm.get('firstName');
  }
  get pass() {
    return this.signForm.get('password');
  }
  get mail() {
    return this.signForm.get('email');
  }
  employee: Employee = new Employee();

  constructor(
    private customerservice: CustomerService,
    private router: Router,
    private toastr: ToastrService
  ) {}
  goToLogin() {
    this.router.navigate(['login']);
  }
  ngOnInit(): void {}
  saveEmployee() {
    this.customerservice.createCustomer(this.signForm.value).subscribe(
      (data) => {
        console.log(data);
        this.router.navigate(['/login']);
        this.toastr.success(
          'Enter your details in login page',
          'Sign UP Successful'
        );
      },
      (error) => console.error()
    );
  }
}
