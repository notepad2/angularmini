import { TestBed } from '@angular/core/testing';

import { ExceptionHandleInterceptor } from './exception-handle.interceptor';

describe('ExceptionHandleInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      ExceptionHandleInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: ExceptionHandleInterceptor = TestBed.inject(ExceptionHandleInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
