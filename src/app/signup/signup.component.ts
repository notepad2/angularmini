import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Employee } from '../employee';
import { CustomerService } from '../customer.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {
  // employee1: Employee = new Employee();
  employee: Employee[] = [];

  constructor(private customerService: CustomerService) {}
  ngOnInit(): void {
    this.getCustomers();
  }
  private getCustomers() {
    this.customerService.getCustomersList().subscribe((data) => {
      this.employee = data;
    });
  }

  page: number = 1;
  length: any;
}
