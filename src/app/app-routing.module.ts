import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';

import { Signup1Component } from './signup1/signup1.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './navigate/home/home.component';
import { HomepageComponent } from './homepage/homepage.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'about',
    pathMatch: 'full',
  },
  {
    component: HomepageComponent,
    path: 'homepage',
  },
  {
    component: AboutComponent,
    path: 'about',
  },
  {
    component: SignupComponent,
    path: 'customers list',
  },
  {
    component: Signup1Component,
    path: 'signup1',
  },
  {
    component: LoginComponent,
    path: 'login',
  },
  {
    path: 'navigate',
    loadChildren: () =>
      import('./navigate/navigate.module').then((m) => m.NavigateModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
