import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomerService } from '../customer.service';
import { Employee } from '../employee';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  gotoSignUp() {
    this.router.navigate(['signup1']);
  }
  cslist = new FormGroup({
    firstName: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });
  get names2() {
    return this.cslist.get('firstName');
  }
  get pass2() {
    return this.cslist.get('password');
  }
  user: Employee = new Employee();

  constructor(
    private loginservice: CustomerService,
    private router: Router,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {}
  userLogin() {
    console.log(this.user);
    this.loginservice.loginUser(this.cslist.value).subscribe((data) => {
      this.router.navigate(['/navigate']),
        this.toastr.success('Login SuccessFul');
    });
  }
}
